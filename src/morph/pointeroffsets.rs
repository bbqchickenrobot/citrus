use crate::exprinfo::ExprInfo;
use c3::expr::*;
use c3::ty::*;
use c3::ty::TyKind as TK;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::API;

use crate::builder::*;

/// Replaces `arr[x]` with `*arr.offset(x)`
pub struct PointerOffsets {
    pub target_api: API,
}

impl PointerOffsets {
    pub fn filter(root: &mut Expr, target_api: API) {
        Self{target_api}.visit(root, Abi::C);
    }
}

impl Filter<Abi> for PointerOffsets {
    fn child_context(&self, expr: &mut Expr, current_abi: Abi) -> Abi {
        match expr.kind {
            K::FunctionDecl(ref mut fun, _) => {
               let change_static_c = self.target_api != API::C && fun.storage == Storage::Static && fun.abi == Abi::C;
               if self.target_api == API::Rust || change_static_c {
                   fun.abi = Abi::Rust;
               }
               fun.abi
            },
            _ => current_abi,
        }
    }

    fn binary(&self, b: &mut BinaryOperator, current_abi: Abi) -> Change {
        // Negative woudl have unary neg
        let is_non_negative = matches!(b.right.ignoring_wrappers().kind, Kind::IntegerLiteral(val, _));

        // TODO: this should be done late, maybe even in tx.rs (just change PointerIndex/ArrayIndex)
        if current_abi != Abi::Rust && b.kind == BK::PointerIndex {
            let loc = b.left.loc;
            let offset = Call {
                callee: MemberRef{
                    name: if is_non_negative {"add"} else {"offset"}.to_owned(),
                    arg: b.left.clone(),
                }.bexpr(loc),
                args: vec![if is_non_negative {(*b.right).clone()} else {Cast {
                    explicit: true,
                    arg: b.right.clone(),
                    ty: Ty {
                        debug_name: None,
                        is_const: false,
                        kind: TK::Typedef("isize".to_owned()),
                    },
                }.expr(loc)}],
            }.expr(loc);
            return Change::Replace(UK::Deref.new(offset).expr(loc));
        }
        Change::Keep
    }
}
