use c3::expr::*;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::exprinfo::*;

/// Move rest of the block into switch's default if all cases return
///
/// Must be ran after switch/case is transformed to match-like AST.
/// Should be ran before `ReturnMatch`
pub struct ReturnToDefault {
}

/// Change `match {return}` to `return match{}`
pub struct ReturnMatch {
}

impl Filter for ReturnMatch {
    fn switch(&self, s: &mut Switch, loc: &Loc, _: ()) -> Change {
        if s.items.iter().all(|i|i.is_unconditionally_returning()) && s.has_default() {
            let mut s = s.clone();
            for c in &mut s.items {
                match c.kind {
                    K::Case(ref mut c) if c.items.len() == 1 => {
                        let val = match c.items[0].kind {
                            K::Return(ref mut val) => val.take(),
                            _ => None,
                        };
                        if let Some(val) = val {
                            c.items[0] = *val;
                        }
                    },
                    _ => {},
                };
            }
            let s = Expr {
                loc: loc.clone(),
                kind: K::Switch(s),
            };
            Change::Modify(K::Return(Some(Box::new(s))))
        } else {
            Change::Keep
        }
    }
}

impl Filter for ReturnToDefault {

    fn block(&self, b: &mut Block, _: ()) -> Change {
        let mut i = 0;
        while i+1 < b.items.len() {
            let can_move_default = match b.items[i].kind {
                K::Switch(ref s) if !s.has_default() => {
                    s.items.iter().all(|i|i.is_unconditionally_returning())
                },
                _ => false,
            };
            if can_move_default {
                let rest = b.items.split_off(i+1);
                let def = Expr{
                    loc: rest[0].loc,
                    kind: K::Case(Case {
                        items: rest,
                        conds: vec![],
                    },
                )};
                match b.items[i].kind {
                    K::Switch(ref mut s) => {
                        s.items.push(def);
                    },
                    _ => unreachable!(),
                };
                break;
            }
            i += 1;
        }
        Change::Keep
    }
}
