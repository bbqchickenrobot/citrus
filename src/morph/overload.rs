use c3::expr::*;
use c3::ty::TyKind as TK;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::builder::*;
use crate::exprinfo::*;

/// C++ overloads look like regular calls, make them operators again
pub struct Overload {
}


impl Filter for Overload {
    fn call(&self, c: &mut Call, _: ()) -> Change {
        if c.args.len() == 2 {
            match c.args[0].ignoring_wrappers().kind {
                K::DeclRef(ref name) => {
                    // TODO:  ~ !  ++ --  ->* -> ( )
                    if let Some(kind) = match name.as_str() {
                        "operator[]" => Some(BK::ArrayIndex),
                        "operator=" => Some(BK::Assign),
                        "operator==" => Some(BK::Eq),
                        "operator!=" => Some(BK::Ne),
                        "operator<" => Some(BK::Lt),
                        "operator>" => Some(BK::Gt),
                        "operator<=" => Some(BK::Le),
                        "operator>=" => Some(BK::Ge),
                        "operator+" => Some(BK::Add),
                        "operator-" => Some(BK::Sub),
                        "operator*" => Some(BK::Mul),
                        "operator/" => Some(BK::Div),
                        "operator%" => Some(BK::Rem),
                        "operator^" => Some(BK::BitXor),
                        "operator&" => Some(BK::BitAnd),
                        "operator|" => Some(BK::BitOr),
                        "operator&&" => Some(BK::And),
                        "operator||" => Some(BK::Or),
                        "operator+=" => Some(BK::AddAssign),
                        "operator-=" => Some(BK::SubAssign),
                        "operator*=" => Some(BK::MulAssign),
                        "operator/=" => Some(BK::DivAssign),
                        "operator%=" => Some(BK::RemAssign),
                        "operator^=" => Some(BK::BitXorAssign),
                        "operator&=" => Some(BK::BitAndAssign),
                        "operator|=" => Some(BK::BitOrAssign),
                        "operator," => Some(BK::Comma),
                        "operator<<" => Some(BK::Shl),
                        "operator>>" => Some(BK::Shr),
                        "operator<<=" => Some(BK::ShlAssign),
                        "operator>>=" => Some(BK::ShrAssign),
                        _ => None,
                    } {
                        let right = Box::new(c.args[1].clone());
                        return Change::Modify(K::BinaryOperator(BinaryOperator {
                            left: c.callee.clone(),
                            right,
                            kind,
                        }));
                    }
                },
                _ => {},
            };
        }
        Change::Keep
    }
}
