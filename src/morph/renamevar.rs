use c3::expr::*;
use c3::ty::*;
use super::{Filter, Change};

/// Rename names that are reserved in Rust to avoid parse errors
pub struct RenameVar {}
impl RenameVar {
    fn is_rust_reserved(name: &str) -> bool {
        match name {
            // minus C keywords
            "as" | "crate" | "fn" | "impl" | "in" | "let" | "loop" | "match" | "mod" | "move" | "mut" | "pub" | "ref" |
            "selfvalue" | "selftype" | "super" | "trait" | "type" | "unsafe" | "use" | "virtual" | "box" | "where" | "proc" |
            "become" | "priv" | "pure" | "unsized" | "yield" | "do" | "abstract" | "final" | "override" | "macro" => true,
            _ => false,
        }
    }

    fn replace_name(name: &mut String) {
        if Self::is_rust_reserved(&name) {
            name.push('_')
        }
    }
}

impl Filter for RenameVar {
    fn designated_init(&self, name: &mut String, _: &mut Expr, _: ()) -> Change {
        Self::replace_name(name);
        Change::Keep
    }

    fn arg(&self, a: &mut Arg) {
        Self::replace_name(&mut a.name);
    }

    fn function_decl(&self, function_decl: &mut FunctionDecl, _: &mut Expr, _: &Loc, _: ()) -> Change {
        Self::replace_name(&mut function_decl.name);
        Change::Keep
    }

    fn decl_ref(&self, f: &mut String, _: ()) -> Change {
        Self::replace_name(f);
        Change::Keep
    }

    fn macro_ref(&self, f: &mut String, _: ()) -> Change {
        Self::replace_name(f);
        Change::Keep
    }

    fn var_decl(&self, a: &mut VarDecl, _: ()) -> Change {
        Self::replace_name(&mut a.name);
        Change::Keep
    }

    fn ty_decl(&self, a: &mut TyDecl, _: ()) -> Change {
        Self::replace_name(&mut a.name);
        Change::Keep
    }
}
