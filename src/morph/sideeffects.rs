use c3::expr::*;
use c3::ty::*;
use c3::expr::UnOpKind as UK;
use c3::expr::BinOpKind as BK;
use c3::expr::Kind as K;
use crate::builder::*;

pub trait SideEffectSplit {
    fn split_side_effects(&self) -> Option<(Vec<Expr>, Expr, Expr)>;
}

fn unary_split(u: &UnaryOperator, loc: Loc) -> Option<(Vec<Expr>, Expr, Expr)> {
    if let Some((some, first, rest)) = u.arg.split_side_effects() {
        Some((some,
            UnaryOperator {
                kind: u.kind,
                arg: Box::new(first),
            }.expr(loc),
            UnaryOperator {
                kind: u.kind,
                arg: Box::new(rest),
            }.expr(loc),
        ))
    } else {
        None
    }
}

impl SideEffectSplit for Expr {
    fn split_side_effects(&self) -> Option<(Vec<Expr>, Expr, Expr)> {
        let loc = self.loc;
        let nop = Expr{
            loc,
            kind: K::TransparentGroup(TransparentGroup{items:vec![]}),
        };
        match self.kind {
            K::TyDecl(..) |
            K::RecordDecl(..) |
            K::FunctionDecl(..) |
            K::FunctionProtoDecl(..) |
            K::TranslationUnit(..) |
            K::ExternC(..) |
            K::InitList(..) |
            K::Switch(..) |
            K::Case(..) |
            K::For(..) |
            K::While(..) |
            K::Return(..) |
            K::Break |
            K::Goto(..) |
            K::Label(..) |
            K::Continue => panic!("Expression too complex to eliminate side effects {:?}", self),
            // Assume it's in a temp scope and hope for the best
            K::VarDecl(ref v) => {
                if let Some(ref init) = v.init {
                    if let Some((side, first, rest)) = init.split_side_effects() {
                        Some((side, VarDecl {
                            ty: v.ty.clone(),
                            init: Some(Box::new(first)),
                            storage: v.storage,
                            name: v.name.clone(),
                        }.expr(loc), nop))
                    } else {
                        None
                    }
                } else {
                    None
                }
            },
            K::Paren(ref e) => if let Some((side, first, rest)) = e.split_side_effects() {
                Some((side, K::Paren(Box::new(first)).expr(loc), K::Paren(Box::new(rest)).expr(loc)))
            } else {
                None
            },
            K::Cast(ref c) => if let Some((side, first, rest)) = c.arg.split_side_effects() {
                Some((side, Cast{
                    arg: Box::new(first),
                    ty:c.ty.clone(),
                    explicit: c.explicit,
                }.expr(loc),
                Cast{
                    arg: Box::new(rest),
                    ty:c.ty.clone(),
                    explicit: c.explicit,
                }.expr(loc)))
            } else {
                None
            },
            K::If(ref i) => {
                let cond = i.cond.split_side_effects();
                let body = i.body.split_side_effects();
                let alt = i.alt.as_ref().map(|a| a.split_side_effects());

                let then_or_else_side_effects = body.is_some() || (alt.is_some() && alt.as_ref().unwrap().is_some());
                if cond.is_some() || then_or_else_side_effects {
                    let mut if_first = i.clone();
                    let mut if_rest = i.clone();
                    let mut side = vec![];
                    if let Some((mut s, first, rest)) = cond {
                        side.append(&mut s);
                        if_first.cond = Box::new(first);
                        if_rest.cond = Box::new(rest);
                    }
                    if then_or_else_side_effects {
                        let (bodyf, bodyr) = if let Some((mut s, first, rest)) = body {
                            side.append(&mut s); // FIXME: invalid! prepend to first-run block instead
                            (first, rest)
                        } else {
                            (nop.clone(), nop)
                        };
                        let (altf, altr) = if let Some(Some((mut s, first, rest))) = alt {
                            side.append(&mut s); // FIXME: invalid! prepend to first-run block instead
                            (Some(Box::new(first)), Some(Box::new(rest)))
                        } else if i.returns_value {
                            (i.alt.clone(), i.alt.clone())
                        } else {
                            (None, None)
                        };
                        side.push(Expr {
                            loc,
                            kind: K::If(If {
                                cond: i.cond.clone(),
                                body: Box::new(bodyf),
                                alt: altf,
                                returns_value: i.returns_value,
                            }),
                        })
                    }
                    Some((side, if_first.expr(loc), if_rest.expr(loc)))
                } else {
                    None
                }
            },
            K::Call(ref c) => {
                let name = "c_"; // FIXME: generate unique;
                let call = VarDecl {
                    name: name.to_owned(),
                    init: None,
                    ty: None,
                    storage: Storage::None,
                }.expr(loc);
                let temp_var = K::declref(name);
                let assign = BK::Assign.new(temp_var.clone().bexpr(loc), Box::new(self.clone()));
                Some((vec![call], assign.expr(loc), temp_var.expr(loc)))
            },
            K::DeclRef(_) |
            K::MacroRef(_) => None,
            K::BinaryOperator(ref b) => {
                match b.kind {
                    BK::MulAssign | BK::DivAssign | BK::RemAssign |
                    BK::AddAssign | BK::SubAssign |
                    BK::AddPtrAssign | BK::SubPtrAssign |
                    BK::ShlAssign | BK::ShrAssign |
                    BK::BitAndAssign | BK::BitXorAssign | BK::BitOrAssign |
                    BK::Assign => {
                        let (side, left_first, left) = b.left.split_side_effects().unwrap_or_else(||{
                            (vec![], (*b.left).clone(), (*b.left).clone())
                        });
                        Some((side, b.kind.new(left_first, b.right.clone()).expr(loc), left))
                    },
                    _ => {
                        let left = b.left.split_side_effects();
                        let right = b.right.split_side_effects();
                        if left.is_some() || right.is_some() {
                            let mut some = vec![];
                            let (leftf, leftr) = if let Some((mut s, first, rest)) = left {
                                some.append(&mut s);
                                (Box::new(first), Box::new(rest))
                            } else {
                                (b.left.clone(), b.left.clone())
                            };
                            let (rightf, rightr) = if let Some((mut s, first, rest)) = right {
                                some.append(&mut s);
                                (Box::new(first), Box::new(rest))
                            } else {
                                (b.right.clone(), b.right.clone())
                            };
                            Some((some, BinaryOperator {
                                    left: leftf, right: rightf,
                                    kind: b.kind,
                                }.expr(loc),
                                BinaryOperator {
                                    left: leftr, right: rightr,
                                    kind: b.kind,
                                }.expr(loc),
                            ))
                        } else {
                            None
                        }
                    }
                }
            },
            K::UnaryOperator(ref u) => match u.kind {
                UK::PostInc |
                UK::PreInc |
                UK::PostDec |
                UK::PreDec |
                UK::PostIncPtr |
                UK::PreIncPtr |
                UK::PostDecPtr |
                UK::PreDecPtr => {
                    let (side, first, arg) = unary_split(u, loc).unwrap_or_else(||{
                        (vec![], (*u.arg).clone(), (*u.arg).clone())
                    });
                    Some((side, u.kind.new(Box::new(first)).expr(loc), arg))
                },
                _ => unary_split(u, loc),
            },

            K::Invalid(..) |
            K::IntegerLiteral(..) |
            K::StringLiteral(..) |
            K::FloatingLiteral(..) |
            K::CharacterLiteral(..) => None,
            K::MemberRef(ref m) => if let Some((side, first, rest)) = m.arg.split_side_effects() {
                Some((side,
                    MemberRef{
                    name: m.name.clone(),
                    arg: first.into(),
                }.expr(loc),
                MemberRef{
                    name: m.name.clone(),
                    arg: rest.into(),
                }.expr(loc)))
            } else {
                None
            },
            // FIXME: could have side effs
            // SizeOf(..) |
            // OffsetOf(..) => None,
            // FIXME: could have side effs
            // CompoundLiteral(..) => None,
            // DesignatedInit(String, Box<Expr>),

            K::TransparentGroup(ref tg) => {
                split_side_effects_items(&tg.items).map(|(side,first,rest)| {
                    (side,
                    TransparentGroup{items:first}.expr(loc),
                    TransparentGroup{items:rest}.expr(loc))
                })
            },
            K::Block(ref b) => {
                split_side_effects_items(&b.items).map(|(side,first,rest)| {
                    (side,
                        Block {
                            items: first,
                            returns_value: b.returns_value,
                        }.expr(loc),
                        Block {
                            items: rest,
                            returns_value: b.returns_value,
                        }.expr(loc),
                    )
                })
            },
            _ => {
                eprintln!("Ignored potential side-effects of {:?}", self);
                None
            },
        }
    }
}

fn split_side_effects_items(exprs: &[Expr]) -> Option<(Vec<Expr>, Vec<Expr>, Vec<Expr>)> {
    if !exprs.is_empty() {
        let mut side = vec![];
        let mut first_items = vec![];
        let mut rest_items = vec![];
        let mut has_side_effects = false;
        for i in exprs {
            if let Some((mut s, first, rest)) = i.split_side_effects() {
                has_side_effects = true;
                side.append(&mut s);
                first_items.push(first);
                rest_items.push(rest);
            } else {
                first_items.push(i.clone());
                rest_items.push(i.clone());
            }
        }
        if has_side_effects {
            return Some((side, first_items, rest_items));
        }
    }
    None
}

#[test]
fn side() {
    use crate::builder::*;
    let loc = Loc::dummy();
    let v_inc = UK::PostInc.new(K::declref("v").bexpr(loc)).expr(loc);
    let (side, f, v) = v_inc.split_side_effects().unwrap();
    assert_eq!(0, side.len());
    match f.kind {
        K::UnaryOperator(..) => true,
        _ => panic!("bad"),
    };
    match v.kind {
        K::DeclRef(..) => true,
        _ => panic!("bad"),
    };

    let adds = BK::AddAssign.new(K::declref("v").bexpr(loc), K::int(1).bexpr(loc)).bexpr(loc);
    let idx = BK::ArrayIndex.new(K::declref("a").bexpr(loc), adds).expr(loc);
    let (side, f, v) = idx.split_side_effects().unwrap();
    assert_eq!(0, side.len());
}
