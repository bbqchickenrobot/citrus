use c3::expr::*;
use c3::ty::TyKind as TK;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::builder::*;
use crate::exprinfo::*;

/// Replace NULL with Rusty nulls
pub struct Null {
}

impl Null {
    fn is_null(e: &Expr) -> bool {
        match e.ignoring_wrappers().kind {
            K::MacroRef(ref m) if m == "NULL" => true,
            _ => false, // TODO: support 0 in ptr context? void cast?
        }
    }

    fn modify_null_compare(e: &mut Expr) {
        let tmp = match e.kind {
            K::BinaryOperator(ref mut b) => {
                let eq = match b.kind {
                    BK::Eq => true,
                    BK::Ne => false,
                    _ => return,
                };
                let arg = if Self::is_null(&b.left) {
                    b.right.clone()
                } else if Self::is_null(&b.right) {
                    b.left.clone()
                } else {
                    return;
                };
                Some((eq, arg))
            },
            _ => None,
        };
        if let Some((eq, arg)) = tmp {
            let check = K::UnaryOperator(UK::IsNull.new(arg));
            e.kind = if eq {
                check
            } else {
                K::UnaryOperator(UK::Not.new(check.bexpr(e.loc)))
            };
        }
    }
}

impl Filter<bool> for Null {
    fn child_context(&self, e: &mut Expr, is_const_ptr: bool) -> bool {
        Self::modify_null_compare(e);

        if let Some(ty) = e.kind.ty() {
            match ty.kind {
                TK::Pointer(ref p) => p.is_const,
                _ => ty.is_const,
            }
        } else {
            is_const_ptr
        }
    }

    fn cast(&self, c: &mut Cast, _: bool) -> Change {
        let replace = match c.ty.kind {
            TK::Pointer(_) |
            TK::ConstantArray(..) |
            TK::VariableArray(..) |
            TK::IncompleteArray(..) |
            TK::BlockPointer => match c.arg.ignoring_wrappers().kind {
                K::IntegerLiteral(0, _) => true,
                _ => false,
            },
            _ => false,
        };
        if replace {
            let loc = c.arg.loc;
            c.arg = Call {
                callee: K::declref(if c.ty.is_const {"ptr::null"} else {"ptr::null_mut"}).bexpr(loc),
                args: vec![],
            }.bexpr(loc);
        }
        Change::Keep
    }

    fn macro_ref(&self, c: &mut String, is_const_ptr: bool) -> Change {
        let loc = Loc::dummy();
        match c.as_str() {
            "NULL" => Change::Modify(K::Call(Call {
                callee: K::declref(if is_const_ptr {"ptr::null"} else {"ptr::null_mut"}).bexpr(loc),
                args: vec![],
            })),
            _ => Change::Keep,
        }
    }
}
