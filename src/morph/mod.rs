//! This performs cleanups of already parsed AST, before final conversion to Rust

use c3::expr::*;
use c3::ty::*;
use c3::expr::Kind as K;
use c3::ty::TyKind as TK;
use std::mem;

mod sideeffects;
mod postinc;
mod anontypes;
mod switch;
mod renamevar;
mod rustyapi;
mod stdfunc;
mod null;
mod rusty;
mod protos;
mod comma;
mod forin;
mod letassign;
mod movelet;
mod returnmatch;
mod whilemod;
mod elaborated;
mod overload;
mod notnot;
mod pointeroffsets;
mod parens;

pub fn filter(root: &mut Expr, options: &crate::Options) {
    anontypes::AnonTypes{}.visit(root, ());
    elaborated::Elaborated::filter(root);
    overload::Overload{}.visit(root, ());
    rusty::IfBlocks{}.visit(root, ()); // Should be before FindPostInc
    switch::FilterSwitchBreak{}.visit(root, ());
    movelet::MoveLet{}.filter(root); // Must be before LetAssign, after FilterSwitchBreak. Should be before FindPostInc
    null::Null{}.visit(root, false);
    rustyapi::RustyAPI::filter(root, options.api); // Must be after NULL, before FindPostInc
    postinc::FindPostInc{}.visit(root, true);
    rusty::CastLiterals{}.visit(root, ()); // Must be after FindPostInc
    notnot::NotNot{}.visit(root, ()); // Best before ForInMod
    stdfunc::StdFunc{}.visit(root, ());
    comma::Comma{}.visit(root, false);
    returnmatch::ReturnToDefault{}.visit(root, ());
    returnmatch::ReturnMatch{}.visit(root, ());
    letassign::LetAssign{}.filter(root); // Best after MoveLet
    protos::Protos{}.filter(root);
    rusty::VarCompound{}.visit(root, None);
    renamevar::RenameVar{}.visit(root, ());
    whilemod::WhileMod{}.visit(root, ());
    forin::ForInMod{}.visit(root, ()); // Must be after FindPostInc
    pointeroffsets::PointerOffsets::filter(root, options.api); // Should be ran after RustyAPI, and not before FindPostInc
    notnot::NotNot{}.visit(root, ()); // Run second time to clean up while/for loops and pointer derefs
    parens::Parens{}.visit(root, ()); // Must be last
}

/// This performs cleanups of already parsed AST, before final conversion to Rust
pub trait Filter<T: Clone = ()> {
    fn child_context<'a>(&self, _expr: &mut Expr, context: T) -> T {
        context
    }

    fn visit_exprs(&self, exprs: &mut Vec<Expr>, context: T) {
        let mut i = 0;
        while i < exprs.len() {
            match self.modify(&mut exprs[i], context.clone()) {
                Change::Keep => {
                    i += 1;
                },
                Change::Modify(kind) => {
                    exprs[i].kind = kind;
                    i += 1;
                },
                Change::Replace(e) => {
                    exprs[i] = e;
                    i += 1;
                },
                Change::ReplaceExprs(items) => {
                    exprs.remove(i);
                    for e in items {
                        exprs.insert(i, e);
                        i += 1;
                    }
                },
            }
        }
    }

    fn visit_args(&self, args: &mut Vec<Arg>, context: T) {
        for a in args.iter_mut() {
            self.visit_ty(&mut a.ty, context.clone());
            self.arg(a);
        }
    }

    fn visit(&self, expr: &mut Expr, context: T) {
        match self.modify(expr, context) {
            Change::Keep => {},
            Change::Modify(kind) => {
                expr.kind = kind;
            },
            Change::Replace(e) => {
                *expr = e;
            },
            Change::ReplaceExprs(items) => {
                *expr = Expr {
                    loc: expr.loc,
                    kind: K::TransparentGroup(TransparentGroup{items}),
                }
            },
        };
    }

    fn modify(&self, expr: &mut Expr, modify_context: T) -> Change {
        let child_visit_context = self.child_context(expr, modify_context.clone());
        let loc = &expr.loc;
        match expr.kind {
            K::VarDecl(ref mut f) => {
                if let Some(ref mut init) = f.init {
                    self.visit(init, child_visit_context);
                }
                if let Some(ty) = f.ty.as_mut() {
                    self.visit_ty(ty, modify_context.clone());
                }
                self.var_decl(f, modify_context)
            },
            K::DeclRef(ref mut name) => {
                self.decl_ref(name, modify_context)
            },
            K::MacroRef(ref mut name) => {
                self.macro_ref(name, modify_context)
            },
            K::TranslationUnit(ref mut tu) => {
                self.visit_translation_unit(tu, child_visit_context);
                self.translation_unit(tu, modify_context)
            },
            K::TransparentGroup(ref mut t) => {
                self.visit_exprs(&mut t.items, child_visit_context);
                self.transparent_group(t, modify_context)
            },
            K::Namespace(_, ref mut items) => {
                self.visit_exprs(items, child_visit_context);
                Change::Keep
            },
            K::ExternC(ref mut items) => {
                self.visit_exprs(items, child_visit_context);
                Change::Keep
            },
            K::FunctionDecl(ref mut f, ref mut body) => {
                self.visit_function_decl(f, body, child_visit_context);
                self.function_decl(f, body, loc, modify_context)
            },
            K::FunctionProtoDecl(ref mut f) => {
                self.visit_args(&mut f.args, modify_context.clone());
                self.function_proto_decl(f, loc, modify_context)
            },
            K::Throw(..) |
            K::Try(..) |
            K::CXXConstructor(..) |
            K::CXXMethod(..) |
            K::CXXDestructor(..) => {
                eprintln!("c++ expr unsupported");
                Change::Keep
            },
            K::InitList(ref mut exprs) => {
                self.visit_init_list(exprs, child_visit_context);
                self.init_list(exprs, modify_context)
            },
            K::DesignatedInit(ref mut name, ref mut expr) => {
                self.visit(expr, child_visit_context);
                self.designated_init(name, expr, modify_context)
            },
            K::Cast(ref mut cast) => {
                self.visit(&mut cast.arg, child_visit_context);
                self.visit_ty(&mut cast.ty, modify_context.clone());
                self.cast(cast, modify_context)
            },
            K::Call(ref mut call) => {
                self.visit_call(call, child_visit_context);
                self.call(call, modify_context)
            },
            K::Block(ref mut block) => {
                self.visit_block(block, child_visit_context);
                self.block(block, modify_context)
            },
            K::ClassDecl(ref mut cls) => {
                self.visit_exprs(&mut cls.items, child_visit_context);
                Change::Keep
            },
            K::CompoundLiteral(ref mut cl) => {
                self.visit_exprs(&mut cl.items, child_visit_context);
                self.visit_ty(&mut cl.ty, modify_context.clone());
                Change::Keep
            },
            K::MemberRef(ref mut m) => {
                self.visit(&mut m.arg, child_visit_context);
                self.member_ref(m, modify_context)
            },
            K::If(ref mut i) => {
                self.visit_if(i, child_visit_context);
                self.if_modify(i, modify_context)
            },
            K::For(ref mut f) => {
                self.visit_for(f, child_visit_context);
                self.for_modify(f, modify_context)
            },
            K::ForIn(ref mut f) => {
                self.visit_for_in(f, child_visit_context);
                self.for_in_modify(f, modify_context)
            },
            K::While(ref mut w) => {
                self.visit_while(w, child_visit_context);
                self.while_modify(w, modify_context)
            },
            K::Switch(ref mut switch) => {
                self.visit_switch(switch, child_visit_context);
                self.switch(switch, loc, modify_context)
            },
            K::Case(ref mut case) => {
                self.visit_case(case, child_visit_context);
                self.case(case, loc, modify_context)
            },
            K::Paren(ref mut expr) => {
                self.visit(expr, child_visit_context);
                Change::Keep
            },
            K::UnaryOperator(ref mut unary) => {
                self.visit_unary(unary, child_visit_context);
                self.unary(unary, modify_context)
            },
            K::BinaryOperator(ref mut binary) => {
                self.visit_binary(binary, child_visit_context);
                self.binary(binary, modify_context)
            },
            K::Return(ref mut expr) => {
                if let Some(ref mut expr) = expr.as_mut() {
                    self.visit(expr, child_visit_context);
                }
                Change::Keep
            },
            K::SizeOf(ref mut s) => {
                if let Some(ref mut expr) = s.arg {
                    self.visit(expr, child_visit_context);
                }
                Change::Keep
            },
            K::Label(_, ref mut expr) => {
                self.visit(expr, child_visit_context);
                Change::Keep
            },
            K::OffsetOf(ref mut ty,_) => {
                self.visit_ty(ty, child_visit_context);
                Change::Keep
            },
            K::RecordDecl(_) => Change::Keep,
            K::Break => self.break_modify(modify_context),
            K::Continue => Change::Keep,
            K::Goto(_) => Change::Keep,
            K::Invalid(_) => Change::Keep,
            K::FloatingLiteral(ref mut val, _) => self.float(val, modify_context),
            K::IntegerLiteral(ref mut val, ref mut ty) => {
                if let Some(ty) = ty.as_mut() {
                    self.visit_ty(ty, modify_context.clone());
                }
                self.integer(val, ty, modify_context)
            },
            K::StringLiteral(_) => Change::Keep,
            K::CharacterLiteral(_) => Change::Keep,
            K::TyDecl(ref mut ty_decl) => {
                self.visit_ty(&mut ty_decl.ty, modify_context.clone());
                self.ty_decl(ty_decl, modify_context)
            },
        }
    }

    fn visit_translation_unit(&self, tu: &mut TranslationUnit, context: T) {
        self.visit_exprs(&mut tu.items, context);
    }

    fn visit_block(&self, b: &mut Block, context: T) {
        self.visit_exprs(&mut b.items, context);
    }

    fn visit_case(&self, case: &mut Case, context: T) {
        self.visit_exprs(&mut case.conds, context.clone());
        self.visit_exprs(&mut case.items, context);
    }

    fn visit_unary(&self, unary: &mut UnaryOperator, context: T) {
        self.visit(&mut unary.arg, context);
    }

    fn visit_binary(&self, binary: &mut BinaryOperator, context: T) {
        self.visit(&mut binary.left, context.clone());
        self.visit(&mut binary.right, context);
    }

    fn visit_call(&self, call: &mut Call, context: T) {
        self.visit_exprs(&mut call.args, context.clone());
        self.visit(&mut call.callee, context);
    }

    fn visit_init_list(&self, exprs: &mut Vec<Expr>, context: T) {
        self.visit_exprs(exprs, context);
    }

    fn visit_function_decl(&self, f: &mut FunctionDecl, body: &mut Expr, context: T) {
        self.visit_args(&mut f.args, context.clone());
        self.visit(body, context);
    }

    fn visit_switch(&self, switch: &mut Switch, context: T) {
        self.visit(&mut switch.cond, context.clone());
        self.visit_exprs(&mut switch.items, context);
    }

    fn visit_if(&self, i: &mut If, context: T) {
        self.visit(&mut i.cond, context.clone());
        self.visit(&mut i.body, context.clone());
        if let Some(ref mut alt) = i.alt {
            self.visit(alt, context);
        }
    }

    fn visit_for(&self, f: &mut For, context: T) {
        if let Some(ref mut init) = f.init {
            self.visit(init, context.clone());
        }
        if let Some(ref mut cond) = f.cond {
            self.visit(cond, context.clone());
        }
        if let Some(ref mut inc) = f.inc {
            self.visit(inc, context.clone());
        }
        self.visit(&mut f.body, context);
    }

    fn visit_for_in(&self, f: &mut ForIn, context: T) {
        self.visit(&mut f.pattern, context.clone());
        self.visit(&mut f.iter, context.clone());
        self.visit(&mut f.body, context);
    }

    fn visit_while(&self, w: &mut While, context: T) {
        self.visit(&mut w.cond, context.clone());
        self.visit(&mut w.body, context);
    }

    fn visit_ty(&self, ty: &mut Ty, context: T) {
        match ty.kind {
            TK::Reference(ref mut inner_ty) |
            TK::Slice(ref mut inner_ty) |
            TK::ConstantArray(_, ref mut inner_ty) |
            TK::VariableArray(_, ref mut inner_ty) |
            TK::IncompleteArray(ref mut inner_ty) |
            TK::Elaborated(_, Some(ref mut inner_ty)) |
            TK::Pointer(ref mut inner_ty) => {
                self.visit_ty(&mut *inner_ty, context.clone());
            },
            TK::Struct(_, ref mut fields) |
            TK::Union(_, ref mut fields) => {
                for f in fields {
                    self.visit_ty(&mut f.ty, context.clone());
                }
            },
            _ => {},
        }
        self.modify_ty(ty, context);
    }

    fn modify_ty(&self, _ty: &mut Ty, _context: T) {
    }

    //////////////////

    fn translation_unit(&self, _tu: &mut TranslationUnit, _context: T) -> Change {
        Change::Keep
    }

    fn init_list(&self, _: &mut Vec<Expr>, _context: T) -> Change {
        Change::Keep
    }

    fn break_modify(&self, _context: T) -> Change {
        Change::Keep
    }

    fn unary(&self, _v: &mut UnaryOperator, _context: T) -> Change {
        Change::Keep
    }

    fn binary(&self, _v: &mut BinaryOperator, _context: T) -> Change {
        Change::Keep
    }

    fn block(&self, _v: &mut Block, _context: T) -> Change {
        Change::Keep
    }

    fn cast(&self, _v: &mut Cast, _context: T) -> Change {
        Change::Keep
    }

    fn call(&self, _v: &mut Call, _context: T) -> Change {
        Change::Keep
    }

    fn member_ref(&self, _v: &mut MemberRef, _context: T) -> Change {
        Change::Keep
    }

    fn if_modify(&self, _v: &mut If, _context: T) -> Change {
        Change::Keep
    }

    fn for_modify(&self, _v: &mut For, _context: T) -> Change {
        Change::Keep
    }

    fn for_in_modify(&self, _v: &mut ForIn, _context: T) -> Change {
        Change::Keep
    }

    fn while_modify(&self, _v: &mut While, _context: T) -> Change {
        Change::Keep
    }

    fn switch(&self, _v: &mut Switch, loc: &Loc, _context: T) -> Change {
        Change::Keep
    }

    fn case(&self, _v: &mut Case, loc: &Loc, _context: T) -> Change {
        Change::Keep
    }

    fn function_decl(&self, _v: &mut FunctionDecl, _body: &mut Expr, loc: &Loc, _context: T) -> Change {
        Change::Keep
    }

    fn arg(&self, _a: &mut Arg) {}

    fn function_proto_decl(&self, _v: &mut FunctionDecl, loc: &Loc, _context: T) -> Change {
        Change::Keep
    }

    fn var_decl(&self, _v: &mut VarDecl, _context: T) -> Change {
        Change::Keep
    }

    fn decl_ref(&self, _f: &mut String, _context: T) -> Change {
        Change::Keep
    }

    fn macro_ref(&self, _f: &mut String, _context: T) -> Change {
        Change::Keep
    }

    fn ty_decl(&self, _v: &mut TyDecl, _context: T) -> Change {
        Change::Keep
    }

    fn designated_init(&self, _name: &mut String, _v: &mut Expr, _context: T) -> Change {
        Change::Keep
    }

    fn transparent_group(&self, t: &mut TransparentGroup, _context: T) -> Change {
        if t.items.len() == 1 {
            Change::Replace(t.items.remove(0))
        } else {
            let mut items = vec![];
            mem::swap(&mut items, &mut t.items);
            Change::ReplaceExprs(items)
        }
    }

    fn float(&self, _val: &mut f64, _context: T) -> Change {
        Change::Keep
    }

    fn integer(&self, _val: &mut u64, _ty: &mut Option<Ty>, _context: T) -> Change {
        Change::Keep
    }
}

pub enum Change {
    /// Keep the expression as-is
    Keep,
    /// Change kind of expression in-place
    Modify(Kind),
    /// Replace with another expression
    Replace(Expr),
    ReplaceExprs(Vec<Expr>),
}
