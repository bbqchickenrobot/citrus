use c3::expr::*;
use c3::ty::*;
use c3::expr::Kind as K;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use super::{Filter, Change};

use crate::morph::sideeffects::*;
use crate::builder::*;

pub struct FindPostInc {}
impl FindPostInc {
    /// For changing pointer arithmetic `a += 1` to `a = a.add(1)`
    fn ptr_offset(&self, arg: Expr, offset: Expr, is_increment: bool) -> K {
        K::Call(Call{
            callee: Box::new(Expr{loc: arg.loc, kind: K::MemberRef(MemberRef{
                name: if is_increment {"add"} else {"sub"}.to_owned(),
                arg: Box::new(arg),
            })}),
            args: vec![offset],
        })
    }

    fn make_add(&self, var_once: Expr, var_rest: Expr, offset: Expr, kind: BK, is_ptr: bool, loc: Loc) -> K {
        K::BinaryOperator(if is_ptr {
            // pointers need to be re-assigned, `a = a.add(1)`
            let is_increment = kind == BK::AddAssign;
            let off = self.ptr_offset(var_once, offset, is_increment).bexpr(loc);
            BK::Assign.new(var_rest, off)
        } else {
            // ints support `+=`
            kind.new(var_once, offset)
        })
    }

    /// Rusty replacement for various `i++`, `ptr += 2`, etc.
    fn make_pre_int(&self, var_once: &Expr, offset: Expr, kind: BK, is_pre: bool, returns_value: bool, is_ptr: bool) -> K {
        let loc = var_once.loc;
        let (mut items, var_once, var_rest) = if !is_ptr && !returns_value {
            (vec![], var_once.clone(), var_once.clone())
        } else {
            var_once.split_side_effects().unwrap_or_else(|| (vec![], var_once.clone(), var_once.clone()))
        };

        if !returns_value && items.is_empty() {
            return self.make_add(var_once, var_rest, offset, kind, is_ptr, loc);
        }

        // In Rust assignment is a statement, not an exprssion, so needs
        // to be wrapped in a block to become an expression.
        if !is_pre {
            items.push(VarDecl{
                name: "_t".to_owned(),
                init: Some(Box::new(var_once)),
                ty: None, // FIXME
                storage: Storage::None,
            }.expr(loc));
            items.push(self.make_add(var_rest.clone(), var_rest, offset, kind, is_ptr, loc).expr(loc));
            items.push(K::declref("_t").expr(loc));
            K::Block(Block {
                items,
                returns_value,
            })
        } else {
            items.push(self.make_add(var_once, var_rest.clone(), offset, kind, is_ptr, loc).expr(loc));
            K::BinaryOperator(BK::Comma.new(
                TransparentGroup{items}.bexpr(loc),
                var_rest,
            ))
        }
    }
}

impl Filter<bool> for FindPostInc {

    fn child_context(&self, expr: &mut Expr, returns_value: bool) -> bool {
        match expr.kind {
            K::Block(ref b) => returns_value && b.returns_value,
            K::If(ref b) => returns_value && b.returns_value,
            K::Return(_) | K::Call(_) | K::InitList(_) |
            K::VarDecl(_) | K::DeclRef(_) | K::MacroRef(_) | K::SizeOf(_) | K::OffsetOf(..) |
            K::CompoundLiteral(_) | K::DesignatedInit(..) => true,
            _ => returns_value,
        }
    }

    fn cast(&self, cast: &mut Cast, returns_value: bool) -> Change {
        if !returns_value {
            cast.explicit = false;
        }
        Change::Keep
    }

    fn binary(&self, b: &mut BinaryOperator, returns_value: bool) -> Change {
        let left = b.left.as_ref();
        let right = b.right.as_ref();
        Change::Modify(match b.kind {
            BK::AddPtr       => self.ptr_offset(left.clone(), right.clone(), true),
            BK::SubPtr       => self.ptr_offset(left.clone(), right.clone(), false),
            BK::Assign       => self.make_pre_int(left, right.clone(), BK::Assign,    true,  returns_value, false),
            BK::AddAssign    => self.make_pre_int(left, right.clone(), BK::AddAssign, true,  returns_value, false),
            BK::SubAssign    => self.make_pre_int(left, right.clone(), BK::SubAssign, true,  returns_value, false),
            BK::AddPtrAssign => self.make_pre_int(left, right.clone(), BK::AddAssign, true,  returns_value, true),
            BK::SubPtrAssign => self.make_pre_int(left, right.clone(), BK::SubAssign, true,  returns_value, true),
            _ => return Change::Keep,
        })
    }

    fn unary(&self, b: &mut UnaryOperator, returns_value: bool) -> Change {
        let arg = &b.arg;
        let one = Expr{loc: b.arg.loc, kind:K::IntegerLiteral(1, None)};
        Change::Modify(match b.kind {
            UK::PreInc     => self.make_pre_int(arg, one, BK::AddAssign, true,  returns_value, false),
            UK::PreDec     => self.make_pre_int(arg, one, BK::SubAssign, true,  returns_value, false),
            UK::PostInc    => self.make_pre_int(arg, one, BK::AddAssign, false, returns_value, false),
            UK::PostDec    => self.make_pre_int(arg, one, BK::SubAssign, false, returns_value, false),
            UK::PreIncPtr  => self.make_pre_int(arg, one, BK::AddAssign, true,  returns_value, true),
            UK::PreDecPtr  => self.make_pre_int(arg, one, BK::SubAssign, true,  returns_value, true),
            UK::PostIncPtr => self.make_pre_int(arg, one, BK::AddAssign, false, returns_value, true),
            UK::PostDecPtr => self.make_pre_int(arg, one, BK::SubAssign, false, returns_value, true),
            _ => return Change::Keep,
        })
    }

    fn visit_unary(&self, unary: &mut UnaryOperator, _: bool) {
        self.visit(&mut unary.arg, true);
    }

    fn visit_if(&self, i: &mut If, returns_value: bool) {
        self.visit(&mut i.cond, true);
        self.visit(&mut i.body, i.returns_value && returns_value);
        if let Some(ref mut alt) = i.alt {
            self.visit(alt, i.returns_value && returns_value);
        }
    }

    fn visit_binary(&self, b: &mut BinaryOperator, returns_value: bool) {
        self.visit(&mut b.left, b.kind != BK::Comma);
        self.visit(&mut b.right, returns_value || b.kind != BK::Comma);
    }

    fn visit_function_decl(&self, function_decl: &mut FunctionDecl, body: &mut Expr, _returns_value: bool) {
        self.visit_args(&mut function_decl.args, false);
        self.visit(body, false);
    }

    fn visit_switch(&self, switch: &mut Switch, _returns_value: bool) {
        self.visit(&mut switch.cond, true);
        self.visit_exprs(&mut switch.items, false);
    }

    fn visit_case(&self, case: &mut Case, _returns_value: bool) {
        self.visit_exprs(&mut case.conds, true);
        self.visit_exprs(&mut case.items, false);
    }

    fn visit_for(&self, f: &mut For, _returns_value: bool) {
        if let Some(ref mut init) = f.init {
            self.visit(init, false);
        }
        if let Some(ref mut cond) = f.cond {
            self.visit(cond, true);
        }
        if let Some(ref mut inc) = f.inc {
            self.visit(inc, false);
        }
        self.visit(&mut f.body, false);
    }

    fn visit_while(&self, w: &mut While, _returns_value: bool) {
        self.visit(&mut w.cond, true);
        self.visit(&mut w.body, false);
    }
}
