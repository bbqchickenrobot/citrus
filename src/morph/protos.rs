use c3::expr::*;
use c3::ty::*;
use c3::expr::Kind as K;
use super::{Filter, Change};
use std::collections::HashSet;
use std::rc::Rc;
use std::cell::RefCell;
use crate::builder::*;

/// Clean up function prototypes
pub struct Protos {}

struct Uses {
    referenced: RefCell<HashSet<String>>,
    defined: RefCell<HashSet<String>>,
    proto_dedup: RefCell<HashSet<String>>,
    proto: RefCell<Vec<(Loc, FunctionDecl)>>,
}

#[derive(Clone)]
struct Ctx {
    uses: Rc<Uses>,
}

impl Uses {
    fn dump(&self) -> Vec<Expr> {
        let mut tmp = self.proto.borrow_mut();
        let prototypes: Vec<Expr> = tmp.drain(..)
        .filter(|&(ref loc, ref fun)| {
            // Rust never needs static prototypes
            fun.storage != Storage::Static &&
            // Unused function
            self.referenced.borrow().contains(&fun.name) &&
            // defined in the same file
            !self.defined.borrow().contains(&fun.name)
        })
        .map(|(loc, fun)| Expr {
            loc,
            kind: K::FunctionProtoDecl(fun),
        })
        .collect();
        prototypes
    }
}

impl Protos {
    pub fn filter(&self, expr: &mut Expr) {
        let uses = Rc::new(Uses {
            referenced: RefCell::new(HashSet::new()),
            defined: RefCell::new(HashSet::new()),
            proto_dedup: RefCell::new(HashSet::new()),
            proto: RefCell::new(Vec::new()),
        });
        CollectUses{}.visit(expr, uses.clone());
        self.visit(expr, Ctx {uses});
    }
}

impl Filter<Ctx> for Protos {

    fn translation_unit(&self, tu: &mut TranslationUnit, ctx: Ctx) -> Change {
        let prototypes = ctx.uses.dump();
        if !prototypes.is_empty() {
            // Wrap prototypes in extern "C", because that's
            // the only place in Rust where they can exist
            tu.items.insert(0, Expr {
                loc: prototypes[0].loc,
                kind: K::ExternC(prototypes),
            });
        }
        Change::Keep
    }

    fn function_decl(&self, fun: &mut FunctionDecl, body: &mut Expr, loc: &Loc, ctx: Ctx) -> Change {
        let prototypes = ctx.uses.dump();
        if !prototypes.is_empty() {
            return Change::ReplaceExprs(vec![
                Expr {
                    loc: prototypes[0].loc,
                    kind: K::ExternC(prototypes),
                },
                K::FunctionDecl(fun.clone(), Box::new(body.clone())).expr(*loc),
            ]);
        }
        Change::Keep
    }

    fn function_proto_decl(&self, fun: &mut FunctionDecl, loc: &Loc, ctx: Ctx) -> Change {
        if fun.storage != Storage::Static && !ctx.uses.proto_dedup.borrow().contains(&fun.name) {
            ctx.uses.proto_dedup.borrow_mut().insert(fun.name.clone());
            ctx.uses.proto.borrow_mut().push((loc.clone(), fun.clone()));
        }
        Change::ReplaceExprs(vec![])
    }
}


struct CollectUses {}
impl Filter<Rc<Uses>> for CollectUses {
    fn decl_ref(&self, name: &mut String, uses: Rc<Uses>) -> Change {
        let mut refs = uses.referenced.borrow_mut();
        refs.insert(name.clone());
        Change::Keep
    }

    fn function_decl(&self, fun: &mut FunctionDecl, _body: &mut Expr, _: &Loc, uses: Rc<Uses>) -> Change {
        let mut defs = uses.defined.borrow_mut();
        defs.insert(fun.name.clone());
        Change::Keep
    }
}
