use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::UnOpKind as UK;
use c3::expr::BinOpKind as BK;
use super::{Filter, Change};
use crate::builder::*;
use crate::exprinfo::*;

/// find loops that work as `for i in 0..10`
pub struct ForInMod;

impl ForInMod {
    fn is_var_named(ce: &Expr, wants_name: &str) -> bool {
        match ce.ignoring_wrappers().kind {
            K::DeclRef(ref name) if name == wants_name => true,
            _ => false,
        }
    }

    fn expr_plus_one(expr: &Expr) -> Expr {
        match expr.kind {
            K::IntegerLiteral(val, ref ty) => {
                K::IntegerLiteral(val+1, ty.clone()).expr(expr.loc)
            },
            _ => K::Paren(
                BK::Add.new(
                    expr.clone(),
                    K::int(1).bexpr(expr.loc))
                .bexpr(expr.loc)
            ).expr(expr.loc)
        }
    }

    fn make_range(start: &Expr, end: &Expr, condition: BK, step: BK) -> Option<Box<Expr>> {
        // for(i = 0; i < 10; i++)  0..10
        // for(i = 0; i <= 10; i++) 0..(10+1)
        // for(i = 10; i > 0; i--)  ((0+1)..(10+1)).rev()
        // for(i = 10; i >= 0; i--) (0..(10+1)).rev()

        let exclusive = match condition {
            BK::Lt | BK::Gt | BK::Ne => true,
            BK::Ge | BK::Le => false,
            other => {
                eprintln!("loop with comparison {:#?} not supported", other);
                return None;
            },
        };

        Some(match step {
            BK::AddAssign => {
                let end = if exclusive {
                    end.clone()
                } else {
                    Self::expr_plus_one(end)
                };
                BK::new(BK::HalfOpenRange, start.clone(), end).bexpr(start.loc)
            },
            BK::SubAssign => {
                let start = Self::expr_plus_one(start);
                let end = if exclusive {
                    Self::expr_plus_one(end)
                } else {
                    end.clone()
                };
                let loc = end.loc;
                let range = BK::HalfOpenRange.new(end, start).bexpr(loc);
                let range = K::Paren(range).bexpr(loc);
                Call {
                    callee: MemberRef {
                        name: "rev".to_owned(),
                        arg: range,
                    }.bexpr(loc),
                    args: vec![],
                }.bexpr(loc)
            },
            other => {
                eprintln!("loop with non-increment {:#?} not supported", other);
                return None;
            },
        })
    }

    fn make_for_in(init: Option<&Box<Expr>>, inc_var: &Expr, end: &Expr, body: &Expr, condition: BK, step_dir: BK) -> Option<ForIn> {
        match init.map(|i| &i.ignoring_wrappers().kind) {
            // for int i=0; i < 10; i++
            Some(&K::VarDecl(VarDecl{
                name: ref var_name,
                init: Some(ref start),
                ..
            })) if Self::is_var_named(inc_var, var_name) => {
                Self::make_range(start, end, condition, step_dir)
                .map(|range| {
                    ForIn {
                        pattern: K::declref(var_name.as_str()).bexpr(start.loc),
                        iter: range,
                        body: Box::new(body.clone()),
                    }
                })
            },

            // for ; i < 10; i++
            None => {
                Self::make_range(inc_var, end, condition, step_dir).map(|range| {
                    ForIn {
                        pattern: K::declref(inc_var.var_name().unwrap()).bexpr(inc_var.loc),
                        iter: range,
                        body: Box::new(body.clone()),
                    }
                })
            },
            _ => None,
        }
    }

    fn match_for_in(ce: &mut For) -> Option<ForIn> {
        match *ce {
            For{ref init, cond: Some(ref cond), inc: Some(ref inc), ref body} => {
                match (&inc.ignoring_wrappers().kind, &cond.ignoring_wrappers().kind) {
                    (&K::BinaryOperator(BinaryOperator {
                        kind: step_dir,
                        left: ref inc_var,
                        right: ref step,
                    }),
                    &K::BinaryOperator(BinaryOperator{
                        left: ref cond_var,
                        kind: condition,
                        right: ref end,
                    })) => {
                        match step.kind {
                            K::IntegerLiteral(val, _) if val == 1 => {},
                            _ => return None, // wrong increment (TODO: step_by)
                        };
                        if inc_var.var_name().is_none() || inc_var.var_name() != cond_var.var_name() {
                            return None;
                        }
                        Self::make_for_in(init.as_ref(), inc_var, end, body, condition, step_dir)
                    },
                    _ => None,
                }
            },
            _ => None,
        }
    }
}

impl Filter for ForInMod {
    fn for_modify(&self, expr: &mut For, _: ()) -> Change {
        let for_in = Self::match_for_in(expr);
        if let Some(for_in) = for_in {
            Change::Modify(K::ForIn(for_in))
        } else if let Some(init) = expr.init.take() {
            let loc = init.loc;
            // It's OK to dump init in the parent scope, since C doesn't allow shadowing,
            // so there's nothing to accidentally shadow.
            let mut items = match init.kind {
                K::Block(b) => b.items,
                _ => vec![*init],
            };
            items.push(expr.clone().expr(loc));
            Change::ReplaceExprs(items)
        } else {
            Change::Keep
        }
    }
}
