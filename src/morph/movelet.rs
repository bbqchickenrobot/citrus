use c3::expr::*;
use c3::expr::Kind as K;
use c3::expr::BinOpKind as BK;
use super::{Filter, Change};
use std::collections::HashMap;
use std::collections::hash_map::Entry::*;

use std::rc::Rc;
use std::cell::RefCell;
use std::mem;

/// Move var declaration to the smallest scope the var is used in
pub struct MoveLet {}
impl MoveLet {
    pub fn filter(&self, expr: &mut Expr) {
        self.visit(expr, ());
    }
}

#[derive(Debug)]
struct Use {
    all_in_block: bool,
}

#[derive(Clone)]
struct Uses {
    is_block_child: bool,
    uses: Rc<RefCell<HashMap<String, Use>>>,
}

struct CollectUsage {}
impl Filter<Uses> for CollectUsage {
    fn child_context(&self, e: &mut Expr, mut ctx: Uses) -> Uses {
        match e.kind {
            K::Block(_) | K::For(_) => {
                ctx.is_block_child = true;
            },
            _ => {},
        }
        ctx
    }

    fn decl_ref(&self, name: &mut String, refs: Uses) -> Change {
        // oversimplified (e.g. nested scopes)
        let mut r = refs.uses.borrow_mut();
        if let Some(u) = r.get_mut(name) {
            if !refs.is_block_child {
                u.all_in_block = false;
            }
            return Change::Keep;
        }

        r.insert(name.clone(), Use{
            all_in_block: refs.is_block_child,
        });
        Change::Keep
    }
}

#[derive(Debug)]
enum Seen {
    CandidateItem(usize),
    Useless,
}

impl Filter for MoveLet {
    fn visit_block(&self, b: &mut Block, _: ()) {
        let mut item_contained_uses: Vec<_> = b.items.iter_mut().map(|item| {
            let u = Uses {
                is_block_child: false, // *this* block doesn't count. Needs descendant block to move vars to.
                uses: Rc::new(RefCell::new(HashMap::new()))
            };
            CollectUsage{}.visit(item, u.clone());
            u
        }).collect();

        let mut uses_here = HashMap::new();
        for (i, contains) in item_contained_uses.drain(..).enumerate() {
            let mut t = contains.uses.borrow_mut();
            for (used_var, cond) in t.drain() {
                match uses_here.entry(used_var) {
                    Occupied(mut e) => {
                        e.insert(Seen::Useless);
                    },
                    Vacant(e) => {
                        e.insert(if cond.all_in_block {Seen::CandidateItem(i)} else {Seen::Useless});
                    },
                }
            }
        }

        move_declarations(b, uses_here);

        // it's important to move declarations first, as they can be pushed downward further
        self.visit_exprs(&mut b.items, ());
    }
}

fn move_declarations(b: &mut Block, mut uses_here: HashMap<String, Seen>) {
    let (lets, mut rest): (HashMap<_,_>, _) = b.items.iter_mut().enumerate().partition(|&(i,ref item)| match item.kind {
        K::VarDecl(ref v) => v.init.is_none(), _ => false
    });
    for (_, l) in lets {
        let target = match l.kind {
            K::VarDecl(ref mut v) =>
                if let Some(Seen::CandidateItem(i)) = uses_here.remove(&v.name) {
                    rest.get_mut(&i)
                } else {
                    None
                }
            ,
            _ => unreachable!(),
        };
        if let Some(target) = target {
            let injector = InjectIntoFirstBlock{
                failed: RefCell::new(false),
                expr: RefCell::new(Some(l.clone())),
            };
            injector.visit(target, ());
            if injector.injected() {
                l.kind = K::TransparentGroup(TransparentGroup{items:vec![]});
            }
        }
    }
}

struct InjectIntoFirstBlock {
    failed: RefCell<bool>,
    expr: RefCell<Option<Expr>>,
}

impl InjectIntoFirstBlock {
    fn no_go_zone(&self) {
        if self.expr.borrow_mut().take().is_some() {
            *self.failed.borrow_mut() = true;
        }
    }

    pub fn injected(&self) -> bool {
        *self.failed.borrow() == false && self.expr.borrow().is_none()
    }
}

impl Filter for InjectIntoFirstBlock {
    fn visit_for_in(&self, _: &mut ForIn, _: ()) {
        self.no_go_zone();
    }

    fn visit_switch(&self, _: &mut Switch, _: ()) {
        self.no_go_zone();
    }

    fn visit_if(&self, _: &mut If, _: ()) {
        self.no_go_zone();
    }

    fn visit_for(&self, b: &mut For, _: ()) {
        if b.init.is_none() { // TODO: support merging?
            if let Some(e) = self.expr.borrow_mut().take() {
                b.init = Some(Box::new(e));
            }
        }
        self.no_go_zone();
    }

    fn visit_while(&self, _: &mut While, _: ()) {
        self.no_go_zone();
    }

    // This must go breadth first, otherwise would inject into most-nested block
    fn visit_block(&self, b: &mut Block, ctx: ()) {
        if let Some(e) = self.expr.borrow_mut().take() {
            b.items.insert(0, e);
        } else {
            self.visit_exprs(&mut b.items, ());
        }
    }
}
