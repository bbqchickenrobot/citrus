use c3::expr::*;
use c3::expr::BinOpKind as BK;
use c3::expr::Kind as K;
use super::{Filter, Change};

/// Replaces comma operator with blocks
pub struct Comma;

impl Filter<bool> for Comma {
    fn child_context(&self, expr: &mut Expr, in_comma: bool) -> bool {
        match expr.kind {
            K::BinaryOperator(BinaryOperator{kind: BK::Comma, ..}) => true,
            K::TransparentGroup(_) => in_comma,
            _ => false,
        }
    }

    fn binary(&self, b: &mut BinaryOperator, in_comma: bool) -> Change {
        match b.kind {
            BK::Comma if !in_comma => {
                let mut items = vec![];
                Self::add_comma_exprs(&*b.left, &mut items);
                Self::add_comma_exprs(&*b.right, &mut items);
                Change::Modify(K::Block(Block {
                    items,
                    returns_value: true,
                }))
            },
            _ => Change::Keep,
        }
    }
}

impl Comma {
    fn add_comma_exprs(expr: &Expr, exprs: &mut Vec<Expr>) {
        match expr.kind {
            K::BinaryOperator(BinaryOperator{kind: BK::Comma, ref left, ref right}) => {
                Self::add_comma_exprs(&*left, exprs);
                Self::add_comma_exprs(&*right, exprs);
            },
            K::TransparentGroup(ref t) => {
                for i in &t.items {
                    Self::add_comma_exprs(i, exprs);
                }
            },
            _ => exprs.push(expr.clone()),
        }
    }
}
