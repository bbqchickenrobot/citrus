use c3::expr::*;
use c3::ty::TyKind as TK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::builder::*;

/// Replaces C std functions with Rust equivalents
pub struct StdFunc {}

impl StdFunc {
    fn method(name: &str, mut arg: Expr, loc: Loc) -> K {
        if Self::needs_paren(&arg) {
            arg = Expr {
                loc,
                kind: K::Paren(Box::new(Self::ignoring_casts(&arg).clone())),
            };
        }
        K::MemberRef(MemberRef {
            arg: Box::new(arg),
            name: name.to_owned(),
        })
    }

    fn needs_paren(e: &Expr) -> bool {
        match e.kind {
            K::Block(_) | K::Paren(_) | K::Call(_) | K::DeclRef(_) | K::MemberRef(_) |
            K::IntegerLiteral(..) | K::StringLiteral(_) | K::FloatingLiteral(..) => false,
            K::Cast(Cast{ref arg, ..}) => Self::needs_paren(arg),
            _ => true,
        }
    }

    fn ignoring_casts(e: &Expr) -> &Expr {
        match e.kind  {
            K::Paren(ref e) => e,
            K::Cast(Cast{ref arg, ..}) => &*arg,
            _ => e,
        }
    }
}

impl Filter for StdFunc {
    fn call(&self, c: &mut Call, _: ()) -> Change {
        let kind = {
            let name = match Self::ignoring_casts(&c.callee).kind {
                K::DeclRef(ref n) => n.as_str(),
                _ => return Change::Keep,
            };
            match (c.args.len(), name) {
                (2, n) if n == "powf" || n == "powi" => Self::method(n, c.args.remove(0), c.callee.loc),
                (1, n) if n == "sqrtf" || n == "sqrt" => Self::method("sqrt", c.args.remove(0), c.callee.loc),
                (1, n) if n == "abs" || n == "fabs" => Self::method("abs", c.args.remove(0), c.callee.loc),
                // Weirdo internal clang thing
                (3, n) if n == "__builtin___strcpy_chk" => {
                    c.args.truncate(2);
                    K::declref("strcpy".to_owned())
                },
                (4, n) if n == "__builtin___strncpy_chk" => {
                    c.args.truncate(3);
                    K::declref("strncpy".to_owned())
                },
                (4, n) if n == "__builtin___memset_chk" => {
                    c.args.truncate(3);
                    K::declref("memset".to_owned())
                },
                (4, n) if n == "__builtin___memcpy_chk" => {
                    c.args.truncate(3);
                    K::declref("memcpy".to_owned())
                },
                _ => return Change::Keep,
            }
        };
        c.callee.kind = kind;
        Change::Keep
    }
}
