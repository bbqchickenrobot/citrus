use c3::expr::*;
use c3::ty::*;
use c3::ty::TyKind as TK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;
use std::mem;

/// Fill-in "elaborated" type reference and with a copy of the actual type
pub struct Elaborated {
    types: RefCell<HashMap<String, Ty>>,
}

struct ElaboratedReplace {
    types: RefCell<HashMap<String, Ty>>,
}

impl Elaborated {
    pub fn filter(expr: &mut Expr) {
        let e = Self {
            types: RefCell::new(HashMap::new()),
        };
        e.visit(expr, ());

        ElaboratedReplace{
            types: e.types,
        }.visit(expr, ());
    }

    fn update_types(&self, name: &str, ty: &Ty) {
        let replace = match self.types.borrow().get(name) {
            None => true,
            Some(ty) => match ty.kind {
                TK::Struct(_, ref fields) | TK::Union(_, ref fields) => {
                    // Replace opaque struct definitions with full ones
                    fields.is_empty()
                },
                _ => false,
            },
        };
        if replace {
            self.types.borrow_mut().insert(name.to_owned(), ty.clone());
        }
    }
}

/// Collect all known types
impl Filter for Elaborated {
    fn modify_ty(&self, ty: &mut Ty, _:()) {
        match ty.kind {
            TK::Enum(ref name, _) |
            TK::Struct(ref name, _) |
            TK::Union(ref name, _) => {
                self.update_types(name, ty);
            },
            _ => {},
        };
    }

    fn ty_decl(&self, v: &mut TyDecl, _:()) -> Change {
        self.update_types(&v.name, &v.ty);
        Change::Keep
    }
}

impl Filter for ElaboratedReplace {
    fn modify_ty(&self, ty: &mut Ty, _:()) {
        let replace = match ty.kind {
            TK::Elaborated(ref name, ref mut ty_ref) if ty_ref.is_none() => {
                if let Some(t) = self.types.borrow().get(name) {
                    *ty_ref = Some(Box::new(t.clone()));
                }
                None
            },

            TK::Typedef(ref name) => if let Some(t) = self.types.borrow().get(name) {
                Some(TK::Elaborated(name.clone(), Some(Box::new(t.clone()))))
            } else {
                None
            },

            TK::Struct(ref name, ref mut fields) |
            TK::Union(ref name, ref mut fields) if fields.is_empty() => {
                if let Some(t) = self.types.borrow().get(name) {
                    Some(t.kind.clone())
                } else {
                    None
                }
            },
            _ => None,
        };
        if let Some(replace) = replace {
            ty.kind = replace;
        }
    }
}
