use c3::expr::*;
use c3::ty::*;
use c3::ty::TyKind as TK;
use c3::expr::BinOpKind as BK;
use c3::expr::UnOpKind as UK;
use c3::expr::Kind as K;
use super::{Filter, Change};
use crate::API;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::hash_map::Entry::*;

use crate::builder::*;
use crate::exprinfo::*;

/// Replaces `arr[]` fn args with pointers or slices
pub struct RustyAPI {
    pub target_api: API,
}

impl RustyAPI {
    pub fn filter(root: &mut Expr, target_api: API) {
        Self{target_api}.visit(root, ());
    }
}

impl Filter for RustyAPI {
    fn function_decl(&self, fun: &mut FunctionDecl, body: &mut Expr, _: &Loc, _: ()) -> Change {
        self.change_to_rust(fun);
        self.slice_args(fun, self.check_uses(body));
        Change::Keep
    }

    fn function_proto_decl(&self, fun: &mut FunctionDecl, _: &Loc, _: ()) -> Change {
        self.slice_args(fun, HashMap::new());
        Change::Keep
    }
}

impl RustyAPI {
    fn change_to_rust(&self, fun: &mut FunctionDecl) {
        let change_static_c = self.target_api != API::C && fun.storage == Storage::Static && fun.abi == Abi::C;
        if self.target_api == API::Rust || change_static_c {
            fun.abi = Abi::Rust;
        }
    }

    fn check_uses(&self, body: &mut Expr) -> HashMap<String, Use> {
        let va = VarUses {
            uses: RefCell::new(HashMap::new()),
        };
        va.visit(body, false);
        va.uses.into_inner()
    }

    fn slice_args(&self, fun: &mut FunctionDecl, uses: HashMap<String, Use>) {
        let use_rust_types = fun.abi == Abi::Rust;
        for a in fun.args.iter_mut() {
            let tmp = match a.ty.kind {
                // Rust arrays are by value, but C decays to ptr
                TK::ConstantArray(_, ref elem_ty) => {
                    if use_rust_types || uses.get(&a.name).map_or(false, |u| !u.null_check && !u.arith && u.used_in_non_null_context) {
                        Some(TK::Reference(Box::new(a.ty.clone())))
                    } else if uses.get(&a.name).map_or(false, |u| u.arith) {
                        // pointer arithmetic doesn't preserve array length, so throw it away
                        Some(TK::Pointer(elem_ty.clone()))
                    } else {
                        Some(TK::Pointer(Box::new(a.ty.clone())))
                    }
                },
                TK::Pointer(ref elem_ty) => {
                    if uses.get(&a.name).map_or(false, |u| !u.indexing && !u.arith && !u.null_check && u.used_in_non_null_context) {
                        Some(TK::Reference(elem_ty.clone()))
                    } else {
                        None
                    }
                },
                TK::IncompleteArray(ref elem_ty) => {
                    Some(if use_rust_types {
                        TK::Reference(Box::new(Ty {
                            is_const: a.ty.is_const,
                            debug_name: a.ty.debug_name.clone(),
                            kind: TK::Slice(elem_ty.clone()),
                        }))
                    } else {
                        TK::Pointer(elem_ty.clone())
                    })
                },
                _ => None,
            };
            if let Some(tmp) = tmp {
                a.ty.kind = tmp;
            }
        }
    }
}

#[derive(Default, Debug)]
struct Use {
    indexing: bool,
    arith: bool,
    null_check: bool,
    used_in_non_null_context: bool,
}

struct VarUses {
    uses: RefCell<HashMap<String, Use>>,
}

impl VarUses {
    fn set(&self, expr: &Expr, add: Use) {
        if let Some(name) = expr.var_name() {
            self.set_name(name, add);
        }
    }

    fn set_name(&self, name: &str, add: Use) {
        let mut uses = self.uses.borrow_mut();
        if let Some(b) = uses.get_mut(name) {
            if add.indexing {b.indexing = true;}
            if add.arith {b.arith = true;}
            if add.null_check {b.null_check = true;}
            return;
        }
        uses.insert(name.to_owned(), add);
    }
}

impl Filter<bool> for VarUses {
    fn child_context(&self, e: &mut Expr, assumes_not_null: bool) -> bool {
        match e.kind {
            K::MemberRef(_) => true,
            // it's not necessary to have separate flag for left/right,
            // because constructs like `a += NULL` make no sense anyway.
            K::BinaryOperator(ref b) => match b.kind {
                BK::AddPtr | BK::SubPtr |
                BK::AddPtrAssign | BK::SubPtrAssign |
                BK::ArrayIndex | BK::PointerIndex => true,
                _ => false,
            },
            K::UnaryOperator(ref u) => match u.kind {
                UK::PostInc | UK::PostDec | UK::PreInc | UK::PreDec |
                UK::PostIncPtr | UK::PostDecPtr | UK::PreIncPtr | UK::PreDecPtr |
                UK::Deref => true,
                _ => false,
            },
            _ => assumes_not_null,
        }
    }

    fn visit_call(&self, call: &mut Call, _: bool) {
        self.visit_exprs(&mut call.args, false);
        self.visit(&mut call.callee, true);
    }

    fn decl_ref(&self, name: &mut String, assumes_not_null: bool) -> Change {
        if assumes_not_null {
            self.set_name(name, Use {used_in_non_null_context:true,..Use::default()});
        }
        Change::Keep
    }

    fn binary(&self, b: &mut BinaryOperator, _: bool) -> Change {
        match b.kind {
            BK::ArrayIndex | BK::PointerIndex => {
                self.set(&b.left, Use {indexing:true,..Use::default()});
            },
            BK::AddPtr | BK::SubPtr |
            BK::AddPtrAssign | BK::SubPtrAssign => {
                self.set(&b.left, Use {arith:true,..Use::default()});
            },
            _ => {},
        }
        Change::Keep
    }

    fn unary(&self, b: &mut UnaryOperator, _: bool) -> Change {
        match b.kind {
            UK::IsNull => {
                self.set(&b.arg, Use {null_check:true,..Use::default()});
            },
            UK::PostInc | UK::PostDec | UK::PreInc | UK::PreDec |
            UK::PostIncPtr | UK::PostDecPtr | UK::PreIncPtr | UK::PreDecPtr => {
                self.set(&b.arg, Use {arith:true,..Use::default()});
            },
            _ => {},
        }
        Change::Keep
    }
}
