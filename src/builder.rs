use c3::expr::*;
use c3::expr::Kind as K;

pub trait IntoExpr where Self: Sized {
    fn expr(self, loc: Loc) -> Expr;
    fn bexpr(self, loc: Loc) -> Box<Expr> {
        Box::new(self.expr(loc))
    }
}

impl IntoExpr for K {
    fn expr(self, loc: Loc) -> Expr {
        Expr {
            loc,
            kind: self,
        }
    }
}

macro_rules! impl_into_expr {
    ($name:ident) => {
        impl IntoExpr for $name {
            fn expr(self, loc: Loc) -> Expr {
                Expr {
                    loc,
                    kind: K::$name(self)
                }
            }
        }
    }
}

impl_into_expr! {BinaryOperator}
impl_into_expr! {Block}
impl_into_expr! {Call}
impl_into_expr! {Case}
impl_into_expr! {Cast}
impl_into_expr! {CompoundLiteral}
impl_into_expr! {For}
impl_into_expr! {ForIn}
impl_into_expr! {If}
impl_into_expr! {MemberRef}
impl_into_expr! {TransparentGroup}
impl_into_expr! {UnaryOperator}
impl_into_expr! {VarDecl}
impl_into_expr! {While}
