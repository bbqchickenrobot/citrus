use std::fmt;
use std::io;
use std::mem;
use std::error::Error as StdError;
use c3::Error as C3Error;

use syntex_syntax::codemap::*;

#[derive(Debug)]
pub enum Error {
    Str(String, Option<Box<Error>>),
    Spanned(String, Span, Option<Box<Error>>),
    Located(String, String, Option<String>, Option<Box<Error>>),
    Std(Box<dyn StdError + Send + Sync>),
    Io(io::Error),
}

impl Error {
    pub fn context<S: Into<Error>>(self, s: S) -> Error {
        let s = s.into();
        match self {
            Error::Str(msg, None) => Error::Str(msg, Some(Box::new(s))),
            Error::Str(msg, Some(reason)) => Error::Str(msg, Some(Box::new(reason.context(s)))),
            Error::Spanned(msg, span, None) => Error::Spanned(msg, span, Some(Box::new(s))),
            Error::Spanned(msg, span, Some(reason)) => Error::Spanned(msg, span, Some(Box::new(reason.context(s)))),
            Error::Located(..) |
            Error::Std(_) |
            Error::Io(_) => s.context(self),
        }
    }

    pub fn resolve(&mut self, codemap: &CodeMap, full: bool) {
        match *self {
            Error::Str(_, Some(ref mut r)) |
            Error::Located(_, _, _, Some(ref mut r)) => r.resolve(codemap, full),
            _ => {},
        };

        *self = match *self {
            Error::Spanned(ref msg, span, ref mut reason) => {
                let mut r = None; mem::swap(&mut r, reason);
                let loc = codemap.span_to_string(span);
                let snip = if full {codemap.span_to_snippet(span).ok()} else {None};
                r.as_mut().map(|r|r.resolve(codemap, full && snip.is_none()));
                Error::Located(msg.clone(), loc, snip, r)
            },
            _ => return,
        };
    }
}

pub type Res<T> = Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Str(ref desc, ref reason) => {
                write!(f, "{}", desc)?;
                if let Some(ref reason) = *reason {
                    write!(f, "\n  {}", reason)?;
                }
                Ok(())
            },
            Error::Located(ref msg, ref loc, ref snip, ref reason) => {
                write!(f, "{}\nerror: {}", loc, msg)?;
                if let Some(ref snip) = *snip {
                    write!(f, "\n{}", snip)?;
                }
                if let Some(ref reason) = *reason {
                    write!(f, "\n - {}", reason)?;
                }
                Ok(())
            },
            Error::Spanned(ref msg, ref loc, ref reason) => {
                write!(f, "{} in @{}", msg, loc.lo.0)?;
                if let Some(ref reason) = *reason {
                    write!(f, "\n - {}", reason)?;
                }
                Ok(())
            },
            Error::Std(ref err) => err.fmt(f),
            Error::Io(ref err) => err.fmt(f),
        }
    }
}

impl StdError for Error {

    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match *self {
            Error::Str(_, ref err) |
            Error::Located(_, _, _, ref err) |
            Error::Spanned(_, _, ref err) => err.as_ref().map(|e|e.as_ref() as &dyn StdError),
            Error::Std(ref err) => Some(&**err),
            Error::Io(ref err) => Some(&*err),
        }
    }
}

impl<'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Self {
        Error::Str(s.to_owned(), None)
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error::Str(s, None)
    }
}

impl From<String> for Box<Error> {
    fn from(s: String) -> Self {
        Box::new(Error::Str(s, None))
    }
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

impl<S> From<(S, Span)> for Error where S: Into<String> {
    fn from((s, span): (S, Span)) -> Self {
        Error::Spanned(s.into(), span, None)
    }
}

impl From<C3Error> for Error {
    fn from(s: C3Error) -> Self {
        match s {
            C3Error::Str(a, None) => Error::Str(a, None),
            C3Error::Str(a, Some(b)) => Error::Str(a, Some(Box::new(Error::Std(b.into())))),
            C3Error::Std(a) => Error::Std(a),
            C3Error::Io(a) => Error::Io(a),
        }
    }
}
