
use citrus::{API, Citrus, Options};
use std::env;
use std::path::PathBuf;

fn main() {
    let mut options = Options::default();
    let mut compiler_flags = Vec::new();
    let mut file = None;
    let mut show_help = false;
    let mut allow_args = true;

    for arg in env::args_os().skip(1) {
        let recognized = match arg.to_str() {
            Some("-h" | "--help") if allow_args => {show_help = true; true},
            Some("--api=rust") if allow_args => {options.api = API::Rust; true},
            Some("--api=c") if allow_args => {options.api = API::C; true},
            Some("--") if allow_args => {allow_args = false; true},
            Some(s) if s.starts_with('-') => {compiler_flags.push(s.to_string()); true},
            _ => false,
        };
        if !recognized {
            if file.is_none() {
                file = Some(PathBuf::from(arg));
            } else {
                compiler_flags.push(arg.to_str().expect("needs UTF-8 encoded args").to_owned());
            }
        }
    }

    if show_help || file.is_none() {
        println!(r"Usage: citrus [<citrus options>] <file.c> [<clang flags>]
--api=rust - allow Rust-only types, don't export functions to C
--api=c    - generate functions for C interoperability

https://gitlab.com/citrus-rs/citrus");
        return;
    }

    match Citrus::new(options).and_then(|c| c.transpile_file(file.as_ref().unwrap(), &compiler_flags)) {
        Ok(tx) => {
            println!("{tx}");
        },
        Err(err) => {
            eprintln!("fatal error: {err}");
            std::process::exit(1);
        },
    };
}
