static int test(const int x) {

    switch(x) {
        case 1: case 2:
            return 3;
        case 4:
            return 5;
    }
    return 6;

}
