static foo(void);
void bar(void);
int *complex(const char *x, ...);

void another(void);


int use();
int use() {
    complex(0);
    use();
    another();
}

void another(void);
void another(void);
void another(void);

int *before_f(const char x[]);
static void f() {}

static void use2() {
    before_f(0);
}
