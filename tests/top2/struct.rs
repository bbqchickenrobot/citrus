pub struct Foo {
    // before A comment
    pub a: c_int,
    pub b: c_long,
    // B line comment
    pub c: c_long,
    /* c line comment */
    /* before D comment */
    pub d: c_longlong,
    pub e: c_ulonglong,
    pub f: f32,
    /* before G */
    pub g: f64, // After G
}