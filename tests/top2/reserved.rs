pub static mut ref_: c_int = 1;
unsafe fn mut_(mut in_: c_int, mut super_: i8, mut type_: c_long) {
    let mut pure_ = 1;
    let mut fn_ = pure_ + 2;
    let mut impl_ = fn_ + std::mem::size_of_val(&fn_);
}