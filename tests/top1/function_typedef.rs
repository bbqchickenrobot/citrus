pub struct Foo {
}
pub struct Bar {
}
pub type BarPtr = *mut Bar;
#[no_mangle]
pub unsafe extern "C" fn f1(mut arg1: Foo, arg2: Foo, mut arg3: *const Foo,
                            mut arg4: *const Foo) {
}
#[no_mangle]
pub unsafe extern "C" fn f2(mut arg1: BarPtr, arg2: BarPtr,
                            mut arg3: *const BarPtr,
                            mut arg4: *const BarPtr) {
}
#[no_mangle]
pub unsafe extern "C" fn f3(mut arg1: Foo, arg2: Foo, mut arg3: *const Foo,
                            arg4: Foo) {
}
unsafe fn f1s(mut arg1: Foo, arg2: Foo, mut arg3: *const Foo,
              mut arg4: &mut [Foo]) {
}
unsafe fn f2s(mut arg1: BarPtr, arg2: BarPtr, mut arg3: *const BarPtr,
              mut arg4: &mut [BarPtr]) {
}
unsafe fn f3s(mut arg1: Foo, arg2: Foo, mut arg3: *const Foo,
              mut arg4: &mut [Foo]) {
}