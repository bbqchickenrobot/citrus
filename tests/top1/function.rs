#[no_mangle]
pub unsafe extern "C" fn vararg(mut a1: c_int, ...) { }
#[no_mangle]
pub unsafe extern "C" fn c1(a1: c_int, mut a2: *const c_int,
                            mut arr3: *mut c_int) {
}
#[no_mangle]
pub unsafe extern "C" fn c2(a1: c_int, a2: *const c_int,
                            mut arr3: *const c_int) {
}
unsafe fn c2_static(a1: c_int, a2: *const c_int, mut arr3: &mut [c_int]) { }
#[no_mangle]
pub unsafe extern "C" fn c3(mut a2: *const c_int, a3: *mut c_int) { }