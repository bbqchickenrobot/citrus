extern "C" {
    pub fn other(plain: *mut c_int, sized: *mut [c_int; 5]);
}
#[no_mangle]
pub unsafe extern "C" fn fn_(mut as_arr: *mut c_int,
                             mut sized_arr: *mut [c_int; 5],
                             mut sized_arr_off: *mut c_int,
                             mut offs: *mut c_int, mut nullable: *mut c_int,
                             mut ref_: &mut c_int) {
    if !nullable.is_null() {
        *as_arr.add(1) = *ref_;
        sized_arr_off = sized_arr_off.add(1);
        offs.add(1);
        other(offs, nullable);
    };
}