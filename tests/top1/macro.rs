pub static mut x: c_int = 1;
pub static FOO: c_int = 1 + 1;
pub static CHAR_BIT: c_int = 8;
unsafe fn test() {
    let mut x = FOO + CHAR_BIT;
    let mut y = FOO;
    let mut z = (2 + 1) + (FOO + 1);
}