unsafe fn test_structinit() {
    pub struct Foo {
        pub foo: c_int,
    }
    pub struct Bar {
        pub foo: Foo,
    }
    let mut var = Foo{foo: 1,};
    let mut arr = [Foo{foo: 1,}, Foo{foo: 2,}, Foo{foo: 3,}];
    let mut designated = Foo{foo: 1,};
    let mut arrdesignated = [Foo{foo: 1,}, Foo{foo: 2,}, Foo{foo: 3,}];
    let mut nested = Bar{foo: [1],};
    let mut nestedarr = [Bar{foo: [1],}];
    pub struct Baz {
        pub inlined: c_int,
    }
    let mut complex = [Baz{inlined: 1,}, Baz{inlined: 2,}];
    pub union Quz {
        pub inlined: c_int,
        pub other_field: c_long,
    }
    let mut complex_union =
        [Quz{inlined: 1,}, Quz{inlined: 2,}, Quz{other_field: 3,}];
}