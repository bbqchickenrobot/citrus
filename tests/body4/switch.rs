unsafe fn test_switch() {
    let mut x = 1;
    match x {
        1 => x = 2,
        2 | 3 => x = 3,
        4 | 5 => {
            x = -x;
            x += 1;
            if 1 != 0 {
                x -= 1;
            } else { if 2 != 0 { } else { }; } /*unreachable*/
        }
        _ => { x = -1; loop  { break  } }
    }
    match 1 { 3 | 4 => x = 4, _ => { { x = 1; } x = 4 } };
}