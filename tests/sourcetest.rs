

use std::fs;
use citrus::*;

fn test_dir<F: Fn(String, &str) -> String>(name: &str, options: Options, wrap_c: F) {
    let c = Citrus::new(options).expect("compiler");

    for source_path in fs::read_dir(name).expect(name)
        .map(|e|e.expect("read entry").path())
        .filter(|e|e.extension().map_or(false, |e|e == "c" || e == "m" || e == "cpp")) {

        let source = std::fs::read_to_string(&source_path).expect(".c source");
        let source_name = source_path.file_stem().expect("stem").to_str().expect("stem str");
        let source = wrap_c(source, source_name);
        let expected_path = source_path.with_extension("rs");
        let expected = if expected_path.exists() {std::fs::read_to_string(&expected_path).expect("read .rs")}
            else {format!("file missing {}", expected_path.display())};
        let src_ext = source_path.extension().expect("ext").to_str().expect("ext");
        let shown_path = expected_path.with_extension(format!("fail.{src_ext}"));

        let actual = match c.transpile_source(&source, &shown_path, &[]) {
            Ok(a) => a,
            Err(err) => panic!("transpile: {err}"),
        };
        if actual != expected {
            std::fs::write(expected_path.with_extension("actual.rs"), &actual).expect("write actual");
            std::fs::write(shown_path, &source).expect("write test");
        }
        assert_eq!(expected, actual);
    }
}

#[test]
#[cfg(target_os = "macos")]
fn test_objc() {
    test_dir("tests/objc", Options::default(), |c,_|c);
}

#[test]
fn test_cpp() {
    test_dir("tests/cpp", Options::default(), |c,_|c);
}

#[test]
fn test_body1() {
    test_dir("tests/body1", Options::default(),
        |c,n| format!("static void test_{n}(void){{\n{c};\n}}\n"));
}

#[test]
fn test_body2() {
    test_dir("tests/body2", Options::default(),
        |c,n| format!("static void test_{n}(void){{\n{c};\n}}\n"));
}

#[test]
fn test_body3() {
    test_dir("tests/body3", Options::default(),
        |c,n| format!("static void test_{n}(void){{\n{c};\n}}\n"));
}

#[test]
fn test_body4() {
    test_dir("tests/body3", Options::default(),
        |c,n| format!("static void test_{n}(void){{\n{c};\n}}\n"));
}

#[test]
fn test_top1() {
    test_dir("tests/top1", Options::default(), |c,_|c);
}

#[test]
fn test_top2() {
    test_dir("tests/top2", Options::default(), |c,_|c);
}

#[test]
fn test_top_rustapi() {
    test_dir("tests/top_rustapi", Options {api: API::Rust}, |c,_|c);
}
