unsafe fn test_for() {
    loop  { if 1 != 0 { break ; }; }
    let mut x = 1;
    loop  { continue ; }
    for mut i in 0..10 { }
    for mut i in 0..10 { }
    for mut i in (0..11).rev() { }
    for mut i in (1..11).rev() { }
    for mut i in (1..11).rev() { };
}