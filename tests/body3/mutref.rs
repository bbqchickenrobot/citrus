unsafe fn test_mutref() {
    let mut mutable = 1;
    let mut mutable_ref = &mut mutable;
    let immutable = 1;
    let immutable_ref = &immutable;
}