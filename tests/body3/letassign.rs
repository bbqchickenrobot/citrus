unsafe fn test_letassign() {
    let mut b;
    let mut e = 1;
    let mut f;
    let mut g;
    let mut a = 2;
    let mut d = 3;
    while 1 != 0 { let mut b = 4; f = 0; a = 0; g = 0; }
    let mut c = 5;
    g = 0;
    let mut y;
    let mut z;
    let mut x = { y = { z = 1; z }; y };
    {
        let mut x1;
        let mut y1;
        let mut z1 = { x1 = { y1 = 1; y1 }; x1 };
        { x1 = { y1 = { z1 = 2; z1 }; y1 }; };
    }
    for mut loop_ in 1..10 { }
    {
        let mut conditional;
        if 1 != 0 { conditional = 1; } else { conditional = 0; };
    }
    { let mut inloop; while 0 != 0 { inloop = 1; } inloop = 2; }
    { { { } { { { } { let mut multilevel = 1; }; } { }; }; } { }; };
}