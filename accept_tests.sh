cargo test 2>/dev/null >/dev/null
for i in tests/*/*.actual.rs; do mv "$i" "${i%.actual.rs}.rs"; done
rm -f tests/*/*.fail.[mc]* tests/*/*.fail.rs
cargo test
